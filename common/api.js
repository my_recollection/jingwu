import {
	request
} from '@/common/request.js'

// 登录
export function login(params = {}) {
	return request({
		url: '/api/Mobile/gettoken',
		method: 'post',
		params
	})
}

// 获取列表
export function getLists(params = {}) {
	return request({
		url: '/api/Mobile/index',
		method: 'post',
		params
	})
}

// 获取详情
export function getViews(params = {}) {
	return request({
		url: '/api/Mobile/read',
		method: 'post',
		params
	})
}

// 上传图片
export function uploadImg(params = {}) {
	return request({
		url: '/img-upload',
		method: 'post',
		params
	})
}
// 上传视频
export function uploadVideo(params = {}) {
	return request({
		url: '/video-upload',
		method: 'post',
		params
	})
}

// 保存记录
export function saveData(params = {}) {
	return request({
		url: '/api/Mobile/save',
		method: 'post',
		params
	})
}