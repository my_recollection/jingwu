import {
	baseUrl,
	baseUrl1
} from "@/common/config.js"

// 获取请求头
function getHeaders() {
    let header = {
		"token": 'token',
        "Content-Type": "application/json", //根据自己的数据类型
    }
    return header
}

export function request(options) {
	let url = baseUrl + options.url
	if(options.url == '/img-upload' || options.url == '/video-upload'){
		url = baseUrl1 + options.url
	}
	uni.showLoading()
	options.method = options.method?options.method.toUpperCase():'GET'
	if (!['GET', 'POST', 'PUT', 'DELETE'].includes(options.method)) {
		uni.showToast({
			title: `暂不支持的请求方式: ${options.method}`,
			icon: 'none'
		});
		return
	}
	if(uni.getStorageSync('userinfo').unionid){
		options.params.unionid = uni.getStorageSync('userinfo').unionid
	}
	if(uni.getStorageSync('company_id')){
		options.params.company_id = uni.getStorageSync('company_id')
	}
	return new Promise((resolve, reject) => {
		uni.request({
			url: url,
			method: options.method,
			data: options.params,
			header: getHeaders(),
		}).then(res => {
			uni.hideLoading()
			switch (res[1].statusCode) {
				case 200:
					resolve(res[1].data)
					break
				default:
					reject(res)
					break
			}
		}).catch(
			(response) => {
				uni.hideLoading()
				reject(response)
			}
		)
	})
}
