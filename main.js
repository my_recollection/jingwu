
// #ifndef VUE3
import Vue from 'vue'
import App from './App'

// 格式化时间
Vue.filter('dateFormat', function(val){
	if(isNaN(val)){
		return val
	}
	var date = new Date(val * 1000); //时间戳为10位需*1000，时间戳为13位的话不需乘1000
	var Y = date.getFullYear() + '-';
	var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) + '-';
	var D = (date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) + ' ';
	var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	var s = date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	return Y + M + D + h + m + s;
})
Vue.filter('numFilter', function (value) {
	
	if(value == 0 || value == '') return 0;
    let realVal = parseFloat(value).toFixed(2)
    return realVal
})
Vue.config.productionTip = false
Vue.prototype.$showMsg = function(res){
	uni.showToast({
		title: res.msg,
		icon:'none',
		duration:1500
	})
}


import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif